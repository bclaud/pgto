{
  description = "A Nix-flake-based Kotlin development environment";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

  outputs = { self, nixpkgs }:
    let
      overlays = [
        (final: prev: {
          gradle = prev.gradle.override { java = prev.jdk; };
          kotlin = prev.kotlin.override { jre = prev.jdk; };
        })
      ];
      supportedSystems = [ "x86_64-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin" ];
      forEachSupportedSystem = f: nixpkgs.lib.genAttrs supportedSystems (system: f {
        pkgs = import nixpkgs { inherit system overlays; };
      });
    in
    {
      devShells = forEachSupportedSystem ({ pkgs }: {
        default = pkgs.mkShell {
          packages = with pkgs; [ kotlin gradle gcc ncurses patchelf zlib ];

          shellHook = ''
            export GRADLE_HOME=${pkgs.gradle}/lib/gradle
          '';
        };
      });
    };
}
