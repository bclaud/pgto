package com.claudlabs

import kotlinx.serialization.Serializable
import kotlin.math.pow

public class Pgto(principal: Int, annualInterestRate: Double, numberOfPayments: Int){
    val installment = calculatePgto(principal, annualInterestRate, numberOfPayments )

    private fun calculatePgto(principal: Int, annualInterestRate: Double, numberOfPayments: Int): Int {
        val monthlyInterestRate = annualInterestRate / 12 / 100
        val numerator = (principal / 100) * monthlyInterestRate
        val denominator = 1 - (1 + monthlyInterestRate).pow(-numberOfPayments)

        return ((numerator / denominator) * 100).toInt()
    }
}

@Serializable
data class PgtoDTO(val installment: Int)

@Serializable
data class PgtoRequestDTO(val principal: Int, val annualInterestRate: Double, val numberOfPayments: Int)

