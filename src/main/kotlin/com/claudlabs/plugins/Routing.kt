package com.claudlabs.plugins

import com.claudlabs.Pgto
import com.claudlabs.PgtoDTO
import com.claudlabs.PgtoRequestDTO
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlin.math.pow

fun Application.configureRouting() {
    routing {
        get("/") {
            val pgto = calculatePGTO(10_000.00, 12.12, 32)
            call.respond(mapOf("installment1" to pgto))
        }

        post(path = "/pgto") {
            val pgtoRequest = call.receive<PgtoRequestDTO>()
            val pgto = Pgto(pgtoRequest.principal, pgtoRequest.annualInterestRate, pgtoRequest.numberOfPayments)

            call.respond(PgtoDTO(pgto.installment))
        }
    }
}

fun calculatePGTO(principal: Double, annualInterestRate: Double, numberOfPayments: Int): Double {
    val monthlyInterestRate = annualInterestRate / 12 / 100
    val numerator = principal * monthlyInterestRate
    val denominator = 1 - (1 + monthlyInterestRate).pow(-numberOfPayments)

    return numerator / denominator
}
